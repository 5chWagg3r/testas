create schema `Testas`;
use Testas;
drop schema `Testas`;
drop table leidinioAutoriai;
drop table Autoriai;
drop table leidiniai;
drop table zanrai;
drop table statusai;
drop table Autoriai;
drop table Zurnalas;
drop table Naudotojai;
drop table Atsiliepimai;

create table zanrai(
	ID int primary key auto_increment,
	Pavadinimas varchar(255)
);

create table statusai(
	ID int primary key auto_increment,
    Pavadinimas varchar(255)
);

create table leidiniai(
	ID int primary key auto_increment,
    zanroid int,
    statusoid int,
    IsleidimoData datetime,
    FOREIGN KEY (zanroid) REFERENCES zanrai(ID),
    FOREIGN KEY (statusoid) REFERENCES statusai(ID)
);

create table Autoriai(
	ID int primary key auto_increment,
    vardas varchar(30)
);

create table leidinioAutoriai(
	leidiniaiID int,
    autoriusID int,
	primary key (leidiniaiID, autoriusID),
    FOREIGN KEY (leidiniaiID) REFERENCES leidiniai(ID),
    FOREIGN KEY (autoriusID) REFERENCES Autoriai(ID)
);
create table Naudotojai(
	ID int primary key auto_increment,
    vardas varchar(30)
);
create table Zurnalas(
	ID int primary key,
    NaudotojoID int,
    LeidinioID int,
    PaemimoData datetime,
    GrazinimoData datetime,
    foreign key (NaudotojoID) references Naudotojai(ID),
    foreign key (LeidinioID) references leidiniai(ID)
);

create table Atsiliepimai(
	ID int primary key auto_increment,
    Tekstas varchar(1000),
    NaudotojoID int,
    reitingas float,
    AtsiliepimuData datetime,
    foreign key (NaudotojoID) references Naudotojai(ID)
);

insert into zanrai (Pavadinimas) values
('Drama'),
('Novele'),
('Legenda'),
('Pasaka'),
('Tragedija');

insert into statusai (Pavadinimas) values
('Laisva'),
('Paimta');

insert into Autoriai(vardas) values
('Darius'),
('Vytautas'),
('Lina'),
('Elena'),
('Ruta');

insert into leidiniai(IsleidimoData, statusoid, zanroid)
values
('2017-01-03 12:15:30', 1, 1),
('2018-02-12 09:25:29', 2, 2),
('2018-08-15 20:35:01', 1, 3),
('2015-03-17 03:45:50', 1, 2),
('2013-09-19 04:45:30', 2, 3),
('2005-06-20 05:55:05', 1, 5);

insert into leidinioAutoriai(leidiniaiID, autoriusID) values
(1,1),
(2,2),
(3,3),
(4,4),
(5,5);

Insert into Zurnalas (NaudotojoID, LeidinioID, PaemimoData, GrazinimoData) values
(1, 3, '2019-03-03 10:00:25','2020-03-03 09:35:30'),
(2, 1, '2019-04-04 15:33:25','2020-04-04 10:03:30'),
(3, 2, '2019-05-05 20:51:25','2020-05-05 13:01:10'),
(4, 1, '2019-06-06 09:26:25','2020-06-06 15:10:40'),
(5, 3, '2019-07-07 01:41:25','2020-07-07 16:20:50'),
(3, 2, '2019-08-08 04:12:25' , '2020-08-08 17:13:33');

insert into Naudotojai(vardas) values
('Povilas'),
('Petriukas'),
('Jokubelis'),
('Bevardis'),
('Vardenis'),
('Mikoliukas'),
('PetriukasAntrasis)');

insert into atsiliepimai(Tekstas,reitingas, AtsiliepimuData, NaudotojoID) Values
('Labai Idomu buvo skaityti', 10, '2020-03-02 10:41:30',1),
('Nesamone', 0.5, '2020-03-02 10:41:30',2),
('Skaityciau antra karta', 10, '2020-03-02 10:41:30',3),
('Knyga kaip knyga', 5, '2020-03-02 10:41:30',4);


# 1) ne velesni nei 2016
select IsleidimoData from leidiniai
where IsleidimoData < '2016-00-00 00:00:00';

# 2) ivertinimai > 3.3
select reitingas from Atsiliepimai
where reitingas > 3.3;

# 3) prasideda A trecia B paskutine S
select vardas from Naudotojai
where vardas Like 'A___B%S';

# 4) statusas yra "Laisva"
select * from leidiniai l
inner join statusas s on l.StatusoID = s.ID
where s.Pavadinimas = 'Laisva';

# 5) autoriai ir ju leidiniai
select a.vardas from Autoriai a
left join leidinioAutoriai la ON la.autoriusID = a.ID;

# 6) kurie siandien turi grazinti leidinius
select  n.vardas from Naudotojai n
inner join Zurnalas z on z.NaudotojoID = n.ID
where z.GrazinimoData = '2018-10-22 19:34:00';
# 7) isrinkti naudotojus kurie parase atsiliepimus ir kiek
select Vardas, count (a) from Naudotojai n
inner join Atsiliepimai a on a.NaudotojoID = n.ID;
# 8) -----

# 9) Isrinkti TOP 3
SELECT avg(reitingas) FROM Atsiliepimai
inner join 
GROUP BY ID;
ORDER BY `time` 
LIMIT 3

